import { VoteComponent } from './vote.component';

describe('VoteComponent', () => {
  var component: VoteComponent;

  beforeEach(() => {
    component = new VoteComponent();
  });

  it('should raise voteChange event when upVoted', () => {
    let totalVote = null;
    component.voteChanged.subscribe(tv => {
      totalVote = tv;
    })
    component.upVote();

    expect(totalVote).not.toBeNull();
  });
});
