import { TodosComponent } from './todos.component';
import { TodoService } from './todo.service';
import { of, empty, throwError } from 'rxjs';

describe('TodosComponent', () => {
  let component: TodosComponent;
  let service: TodoService;

  beforeEach(() => {
    service = new TodoService(null);
    component = new TodosComponent(service);
  });

  it('should set todos property with the items recive from server', () => {
    const todos = [1,2,3]
    spyOn(service, 'getTodos').and.callFake(() => {
      return of(todos);
    });

    component.ngOnInit();

    expect(component.todos).toBe(todos)
  });

  it('should call the server when a save a changes when a new todo item is added ', () => {
    let spy = spyOn(service, 'add').and.callFake(t => {
      return of(empty)
    });

    component.add();

    expect(spy).toHaveBeenCalled();
  });

  it('should add new todo returned  from server', () => {
    let todo = {id: 1};
    spyOn(service, 'add').and.returnValues(of(todo));

    component.add();

    expect(component.todos.indexOf(todo)).toBeGreaterThan(-1);
  });

  it('should error message fill when recive error', () => {
    let error = 'somting is wrong!';
    spyOn(service, 'add').and.returnValue(throwError(error));

    component.add();

    expect(component.message).toContain(error)
  });


  it('should call the server to delete a todo if user confirm', () => {
    spyOn(window, 'confirm').and.returnValue(true);
    const spy = spyOn(service, 'delete').and.returnValue(of(empty));

    component.delete(1);

    expect(spy).toHaveBeenCalledWith(1)
  });

  it('should Not call the server to delete a todo if user cancel', () => {
    spyOn(window, 'confirm').and.returnValue(false);
    const spy = spyOn(service, 'delete').and.returnValue(of(empty));

    component.delete(1);

    expect(spy).not.toHaveBeenCalled()
  });
})
