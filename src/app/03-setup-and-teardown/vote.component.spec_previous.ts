import {VoteComponent} from './vote.component_previous';

describe('VoteComponent', () => {

  //arrange
  let componet: VoteComponent;

  beforeEach(() => {
    componet = new VoteComponent();
  });

  it('should increase totalVote when call upVote', () => {
    //act
    componet.upVote();

    //assert
    expect(componet.totalVotes).toBe(1);
  });

  it('should decrease totalVote when call downVote', () => {
    //act
    componet.downVote();

    //assert
    expect(componet.totalVotes).toBe(-1);
  });

})
