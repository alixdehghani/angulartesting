import { compute } from './compute';
describe('compute', () => {
 it('should be 0 for negative', () => {
   const result = compute(-1);
   expect(result).toBe(0);
 });
 it('should be increase for poseitive', () => {
   const result = compute(1);
   expect(result).toBeGreaterThan(1)
 })
})
